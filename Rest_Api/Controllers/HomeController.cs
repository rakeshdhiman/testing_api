﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using QRCoder;
using Rest_Api.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Rest_Api.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
       string BaseUrl = "http://api.makeyouonline.com/";
       // string BaseUrl = "https://localhost:44311/";// http://localhost:50358/Home/Index
        static string _userName = "",_genToken="";
        static string _webHeading = "";
        static string _LoginUserToken = "",_LoginUserName="",_LoginUserID="";
        List<LoginModel> _LoginList = new List<LoginModel>();
        TestingAPIEntities database = new TestingAPIEntities();
        public async Task<ActionResult> Index()
        {
            List<LoginModel> LoginInfo = new List<LoginModel>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.GetAsync("api/RestAPIS");
                if(Res.IsSuccessStatusCode)
                {
                    var LoginResponce = Res.Content.ReadAsStringAsync().Result;
                    // LoginInfo = JsonConvert.DeserializeObject<List<LoginModel>>(LoginResponce);
                    ViewBag.Message = JsonConvert.DeserializeObject(LoginResponce);
                }
               
            }
            return View();
        }

        [HttpPost]
        public JsonResult AdminGenerateToken(LoginModel model)
        {           
            var Tokens = GenerateTokens(model);
            string status = Tokens;
            if(status.Contains("user"))
            {
                status = "1";               
            }
            else
            {
                status = Tokens;
                _userName = model.UserName;
                _genToken = status;
            }
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        private  string GenerateTokens(LoginModel model)
        {
            string tokens = "";
            using (var client = new HttpClient())
            {
                string postapi = "api/RestAPIS/CheckCredantials";
                BaseUrl = BaseUrl + postapi;
                client.BaseAddress = new Uri(BaseUrl);
                client.DefaultRequestHeaders.Clear();
                // Http POSt
                var PostTask = client.PostAsJsonAsync<LoginModel>("Home",model);
                PostTask.Wait();
                var result = PostTask.Result;
                if(result.IsSuccessStatusCode)
                {
                    var responce = result.Content.ReadAsStringAsync().Result;
                    tokens = responce;
                }
               
            }
            return tokens;
        }

        [HttpGet]
        public ActionResult AdminIndex()
        {
            if(_userName=="" && _genToken=="")
            {
                return RedirectToAction("Index");
            }
            ViewBag.userName = _userName;
            ViewBag.Tokens = _genToken;
            return View();
        }

        [HttpPost]
        public JsonResult CreateWebSite(WebSiteModels webModel)
        {
            int webstatus = 0;
            if(webModel.webHeading==null)
            {
                webstatus = 0;
            }
            else
            {
                _webHeading = webModel.webHeading;
                webstatus = 1;
            }
            return Json(webstatus, JsonRequestBehavior.AllowGet);
        }
       
        public ActionResult HomePage()
        {
            if(_webHeading=="")
            {
                ViewBag.webHeading = "Hello,I am website Heading";
            }
            else
            {
                ViewBag.webHeading = _webHeading;
            }           
            return View();
        }

        #region For User SignUp
        public ActionResult UserSignUp()
        {
            return View();
        }

        [HttpPost]
        public JsonResult UserRegistration(UserLogin userregistrationmodel)
        {
            int status = 0;
            string tokens = "";
            try
            {               
                using (var client = new HttpClient())
                {
                    string postapi = "api/UserRegistration/RegisterNewUser";
                    BaseUrl = BaseUrl + postapi;
                    client.DefaultRequestHeaders.Clear();
                    client.BaseAddress = new Uri(BaseUrl);                   
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    try
                    {
                        HttpResponseMessage response = client.PostAsJsonAsync(BaseUrl, userregistrationmodel).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            var responce = response.Content.ReadAsStringAsync().Result;
                            tokens = responce;
                            status = 1;
                        }
                        else
                        {

                        }
                        if (tokens.Contains("registered"))
                        {
                            status = 0;
                        }
                    }

                    catch (Exception ex)
                    {
                        //lblmsg.Text = ex.Message.ToString();
                    }
                   
                }
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region For User Login
        public ActionResult UserLogin()
        {
           
            return View();
        }
        [HttpPost]
        public JsonResult GetUserToken(LoginModel model)
        {
             var st = GetTokens(model);
          
            return Json(st, JsonRequestBehavior.AllowGet);
        }
        private string GetTokens(LoginModel model)
        {
            string status = "";
            string tokens = "";
            using (var client = new HttpClient())
            {
                string postapi = "api/UserRegistration/GetSavedtokens";
                BaseUrl = BaseUrl + postapi;
                client.DefaultRequestHeaders.Clear();
                client.BaseAddress = new Uri(BaseUrl);                
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));             
                try
                {
                    HttpResponseMessage response = client.PostAsJsonAsync(BaseUrl, model).Result;
                  
                    if (response.IsSuccessStatusCode)
                    {
                       
                        var responce = response.Content.ReadAsStringAsync().Result;
                        var data = JsonConvert.DeserializeObject<UserLogin>(responce);                        
                        tokens = data.UserToken;
                        status = tokens;
                        _LoginUserToken = tokens;
                        _LoginUserName = data.UserName;
                        _LoginUserID = data.UserID.ToString();
                    }
                    else
                    {
                       
                    }
                    if (tokens.Contains("registered"))
                    {
                        status = "1";
                    }
                }

                catch (Exception ex)
                {
                   
                    //lblmsg.Text = ex.Message.ToString();
                }
              
            }
            return status;
        }

        #endregion

        #region For After User Login then Go to Student Management System
        public ActionResult StudentMgmSystem()
        {
            if(_LoginUserToken=="" || _LoginUserName=="")
            {
                return RedirectToAction("HomePage");
            }
            ViewBag.UserID = _LoginUserID;
            return View();
        }
       
        [HttpPost]
        public JsonResult GetStudentList()
        {
            List<StudentRegistration> stdlist = new List<StudentRegistration>();
            try
            {
                StudentRegistration getList = new StudentRegistration();
                getList.UserID = Convert.ToInt32(_LoginUserID);
                using (var client = new HttpClient())
                {
                    string postapi = "api/UserRegistration/GetStudentListByUserID";
                    BaseUrl = BaseUrl + postapi;
                    client.DefaultRequestHeaders.Clear();
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    try
                    {
                        HttpResponseMessage response = client.PostAsJsonAsync(BaseUrl,getList).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            var responce = response.Content.ReadAsStringAsync().Result;
                            var studentlist = JsonConvert.DeserializeObject<List<StudentRegistration>>(responce);
                            if (studentlist.Count == 0)
                            {

                            }
                            else
                            {
                                for (int i = 0; i < studentlist.Count; i++)
                                {
                                    StudentRegistration studentRegistration = new StudentRegistration()
                                    {
                                        UserID = studentlist[i].UserID,
                                        BatchID = studentlist[i].BatchID,
                                        StateID = studentlist[i].StateID,
                                        StudentID = studentlist[i].StudentID,
                                        SubjectID = studentlist[i].SubjectID,
                                        FatherName = studentlist[i].FatherName,
                                        MobileNumber = studentlist[i].MobileNumber,
                                        MotherName = studentlist[i].MotherName,
                                        GenderID = studentlist[i].GenderID,
                                        Name = studentlist[i].Name,
                                        Village = studentlist[i].Village,
                                        Image = studentlist[i].Image
                                    };
                                    stdlist.Add(studentRegistration);
                                }
                            }
                        }                      
                    }

                    catch (Exception ex)
                    {
                        //lblmsg.Text = ex.Message.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }
            return Json(stdlist, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Common Data for Dropdown eg class,section,state,gender etc.
        public JsonResult StudentSubjectList()
        {
            List<SelectListItem> tmpSubjectList = new List<SelectListItem>();
            tmpSubjectList.Add(new SelectListItem { Text = "Computer Basic", Value = "1" });
            tmpSubjectList.Add(new SelectListItem { Text = "Web Designing", Value = "2" });
            tmpSubjectList.Add(new SelectListItem { Text = "English Speaking", Value = "3" });
            tmpSubjectList.Add(new SelectListItem { Text = "ASP.NET", Value = "4" });
            tmpSubjectList.Add(new SelectListItem { Text = "Xamarin", Value = "5" });
            return Json(tmpSubjectList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult StudentTimeList()
        {
            List<SelectListItem> tmpTimeList = new List<SelectListItem>();
            tmpTimeList.Add(new SelectListItem { Text = "08:00 to 09:00", Value = "1" });
            tmpTimeList.Add(new SelectListItem { Text = "09:00 to 10:00", Value = "2" });
            tmpTimeList.Add(new SelectListItem { Text = "10:00 to 11:00", Value = "3" });
            tmpTimeList.Add(new SelectListItem { Text = "11:00 to 12:00", Value = "4" });
            tmpTimeList.Add(new SelectListItem { Text = "12:00 to 01:00", Value = "5" });
            tmpTimeList.Add(new SelectListItem { Text = "01:00 to 02:00", Value = "6" });
            tmpTimeList.Add(new SelectListItem { Text = "02:00 to 03:00", Value = "7" });
            tmpTimeList.Add(new SelectListItem { Text = "03:00 to 04:00", Value = "8" });
            tmpTimeList.Add(new SelectListItem { Text = "04:00 to 05:00", Value = "9" });
            tmpTimeList.Add(new SelectListItem { Text = "05:00 to 06:00", Value = "10" });
            return Json(tmpTimeList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult StudentGenderList()
        {
            List<SelectListItem> tmpGenderList = new List<SelectListItem>();
            tmpGenderList.Add(new SelectListItem { Text = "Male", Value = "1" });
            tmpGenderList.Add(new SelectListItem { Text = "Female", Value = "2" });
            tmpGenderList.Add(new SelectListItem { Text = "Other", Value = "3" });           
            return Json(tmpGenderList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult StudentStateList()
        {
            List<SelectListItem> tmpStateList = new List<SelectListItem>();
            tmpStateList.Add(new SelectListItem { Text = "Bihar", Value = "1" });
            tmpStateList.Add(new SelectListItem { Text = "Uttar Pradesh", Value = "2" });
            tmpStateList.Add(new SelectListItem { Text = "Madhya Pradesh", Value = "3" });
            tmpStateList.Add(new SelectListItem { Text = "Haryana", Value = "4" });
            tmpStateList.Add(new SelectListItem { Text = "Punjab", Value = "5" });
            tmpStateList.Add(new SelectListItem { Text = "Rajsthan", Value = "6" });
            tmpStateList.Add(new SelectListItem { Text = "New Delhi", Value = "7" });
            tmpStateList.Add(new SelectListItem { Text = "Maharashtra", Value = "8" });
            tmpStateList.Add(new SelectListItem { Text = "Odisha", Value = "9" });
            tmpStateList.Add(new SelectListItem { Text = "Chhatisgarh", Value = "10" });
            return Json(tmpStateList, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Add New Student 
        public ActionResult AddNewStudent()
        {
            if (_LoginUserToken == "" && _LoginUserName == "")
            {
                return RedirectToAction("HomePage");
            }

            return View();
        }       
        [HttpPost]
        public JsonResult SaveStudentData(string Name, string FatherName, string MotherName, string MobileNumber, string Village,
            string SubjectID, string BatchID, string StateID, string GenderID)
        {           
            int status = 0;
            string tokens = "";
            byte[] imgArray = new byte[] { };          
            if (Request.Files != null)
            {
                var _FileType = Request.Files[0].ContentType;
                var _FileName = Request.Files[0].FileName;
                Bitmap bitm = new Bitmap(_FileName);
                using (MemoryStream mStream = new MemoryStream())
                {
                    bitm.Save(mStream, bitm.RawFormat);
                    imgArray = mStream.ToArray();
                }
            }
            try
            {              

                using (var client = new HttpClient())
                {
                    string postapi = "api/UserRegistration/AddNewStudent";
                    BaseUrl = BaseUrl + postapi;
                    client.DefaultRequestHeaders.Clear();
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    try
                    {
                        StudentRegistration studentRegistration = new StudentRegistration()
                        {
                            UserID = Convert.ToInt32(_LoginUserID),
                            Name=Name,
                            FatherName=FatherName,
                            MobileNumber=MobileNumber,
                            MotherName=MotherName,
                            StateID= Convert.ToInt32(StateID),
                            BatchID= Convert.ToInt32(BatchID),
                            SubjectID= Convert.ToInt32(SubjectID),
                             Village=Village,
                             GenderID= Convert.ToInt32(GenderID),
                             Image=imgArray
                            
                        };
                        HttpResponseMessage response = client.PostAsJsonAsync(BaseUrl, studentRegistration).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            var responce = response.Content.ReadAsStringAsync().Result;
                            tokens = responce;
                            if(tokens=="0")
                            {
                                status = 0;
                            }
                            else
                            {
                                status = 1;
                            }
                            
                        }
                        else
                        {

                        }
                        if (tokens.Contains("registered"))
                        {
                            status = 0;
                        }
                    }

                    catch (Exception ex)
                    {
                        //lblmsg.Text = ex.Message.ToString();
                    }

                }
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        public byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }

        #endregion

        #region Generate Bar Code
        [HttpPost]
       public ActionResult GenBarcode(string barcode)
        {      
            // For Bar Code
            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (Bitmap bitMap = new Bitmap(barcode.Length * 40, 80))
                {
                    using (Graphics graphics = Graphics.FromImage(bitMap))
                    {
                        Font oFont = new Font("IDAutomationHC39M", 16);
                        PointF point = new PointF(2f, 2f);
                        SolidBrush whiteBrush = new SolidBrush(Color.White);
                        graphics.FillRectangle(whiteBrush, 0, 0, bitMap.Width, bitMap.Height);
                        SolidBrush blackBrush = new SolidBrush(Color.DarkBlue);
                        graphics.DrawString("*" + barcode + "*", oFont, blackBrush, point);
                    }
                    bitMap.Save(memoryStream, ImageFormat.Jpeg);
                    ViewBag.BarcodeImage = "data:image/png;base64," + Convert.ToBase64String(memoryStream.ToArray());

                }
            }
            // For QR Code
            using (MemoryStream ms = new MemoryStream())
            {
                QRCodeGenerator qrGenerator = new QRCodeGenerator();
                QRCodeGenerator.QRCode qrCode = qrGenerator.CreateQrCode(barcode, QRCodeGenerator.ECCLevel.Q);
                using (Bitmap bitMap = qrCode.GetGraphic(20))
                {
                    bitMap.Save(ms, ImageFormat.Png);
                    ViewBag.QRCodeImage = "data:image/png;base64," + Convert.ToBase64String(ms.ToArray());
                }
            }
            return View("GetSecurity");
        }

        #endregion

        #region Generate QR Code
        [HttpPost]
        public ActionResult GenQRcode(string qrcode)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                QRCodeGenerator qrGenerator = new QRCodeGenerator();
                QRCodeGenerator.QRCode qrCode = qrGenerator.CreateQrCode(qrcode, QRCodeGenerator.ECCLevel.Q);
                using (Bitmap bitMap = qrCode.GetGraphic(20))
                {
                    bitMap.Save(ms, ImageFormat.Png);
                    ViewBag.QRCodeImage = "data:image/png;base64," + Convert.ToBase64String(ms.ToArray());
                }
            }

            return View("HomePage");
        }
        #endregion

        #region For Generate Security (Password,Barcode,QRCode etc)
        public ActionResult GetSecurity()
        {
            return View();
        }
        #endregion

        #region Save Student Credential for AddMarks,View Details etc
              
        public ActionResult StudentMarks(string stdid)
        {
            if(_LoginUserID=="" || _LoginUserToken=="")
            {
                return View("UserLogin");
            }
            else
            {
                ViewBag.StudentID = stdid;
            }           
            return View();
        }
        [HttpPost]
        public JsonResult GetStudentInfo(string StudentID)
        {
            List<StudentRegistration> stdlist = new List<StudentRegistration>();
            try
            {
                StudentRegistration getList = new StudentRegistration();
                getList.StudentID =Convert.ToInt32(StudentID);
                using (var client = new HttpClient())
                {
                    string postapi = "api/UserRegistration/GetStudentInfoByStudentID";
                    BaseUrl = BaseUrl + postapi;
                    client.DefaultRequestHeaders.Clear();
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    try
                    {
                        HttpResponseMessage response = client.PostAsJsonAsync(BaseUrl, getList).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            var responce = response.Content.ReadAsStringAsync().Result;
                            var studentlist = JsonConvert.DeserializeObject<List<StudentRegistration>>(responce);
                            if (studentlist.Count == 0)
                            {

                            }
                            else
                            {
                                for (int i = 0; i < studentlist.Count; i++)
                                {
                                    StudentRegistration studentRegistration = new StudentRegistration()
                                    {
                                        UserID = studentlist[i].UserID,
                                        BatchID = studentlist[i].BatchID,
                                        StateID = studentlist[i].StateID,
                                        StudentID = studentlist[i].StudentID,
                                        SubjectID = studentlist[i].SubjectID,
                                        FatherName = studentlist[i].FatherName,
                                        MobileNumber = studentlist[i].MobileNumber,
                                        MotherName = studentlist[i].MotherName,
                                        GenderID = studentlist[i].GenderID,
                                        Name = studentlist[i].Name,
                                        Village = studentlist[i].Village,
                                        Image = studentlist[i].Image
                                    };
                                    stdlist.Add(studentRegistration);
                                }
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        //lblmsg.Text = ex.Message.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }
            return Json(stdlist, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region for carosul testing only view
        public ActionResult TestingView()
        {
            return View();
        }
        #endregion

        #region For Search StudentList By Name  
        [HttpPost]
        public JsonResult SearchByName(SearchModel model)
        {
            List<StudentRegistration> stdlist = new List<StudentRegistration>();
            stdlist.Clear();
            try
            {
                StudentRegistration getList = new StudentRegistration();
                getList.Name =model.Name;
                getList.UserID =Convert.ToInt32(model.UserID);
                using (var client = new HttpClient())
                {
                    string postapi = "api/UserRegistration/GetStudentListByName";
                    BaseUrl = BaseUrl + postapi;
                    client.DefaultRequestHeaders.Clear();
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    try
                    {
                        HttpResponseMessage response = client.PostAsJsonAsync(BaseUrl, getList).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            var responce = response.Content.ReadAsStringAsync().Result;
                            var studentlist = JsonConvert.DeserializeObject<List<StudentRegistration>>(responce);
                            if (studentlist.Count == 0)
                            {

                            }
                            else
                            {
                                for (int i = 0; i < studentlist.Count; i++)
                                {
                                    StudentRegistration studentRegistration = new StudentRegistration()
                                    {
                                        UserID = studentlist[i].UserID,
                                        BatchID = studentlist[i].BatchID,
                                        StateID = studentlist[i].StateID,
                                        StudentID = studentlist[i].StudentID,
                                        SubjectID = studentlist[i].SubjectID,
                                        FatherName = studentlist[i].FatherName,
                                        MobileNumber = studentlist[i].MobileNumber,
                                        MotherName = studentlist[i].MotherName,
                                        GenderID = studentlist[i].GenderID,
                                        Name = studentlist[i].Name,
                                        Village = studentlist[i].Village,
                                        Image = studentlist[i].Image
                                    };
                                    stdlist.Add(studentRegistration);
                                }
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        //lblmsg.Text = ex.Message.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }
            return Json(stdlist, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region For Search student List By Mobile Number
        [HttpPost]
        public JsonResult SearchByMobileNumber(SearchModel model)
        {
            List<StudentRegistration> stdlist = new List<StudentRegistration>();
            stdlist.Clear();
            try
            {
                StudentRegistration getList = new StudentRegistration();
                getList.MobileNumber = model.MobileNumber;
                getList.UserID = Convert.ToInt32(model.UserID);
                using (var client = new HttpClient())
                {
                    string postapi = "api/UserRegistration/GetStudentListByMobileNumber";
                    BaseUrl = BaseUrl + postapi;
                    client.DefaultRequestHeaders.Clear();
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    try
                    {
                        HttpResponseMessage response = client.PostAsJsonAsync(BaseUrl, getList).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            var responce = response.Content.ReadAsStringAsync().Result;
                            var studentlist = JsonConvert.DeserializeObject<List<StudentRegistration>>(responce);
                            if (studentlist.Count == 0)
                            {

                            }
                            else
                            {
                                for (int i = 0; i < studentlist.Count; i++)
                                {
                                    StudentRegistration studentRegistration = new StudentRegistration()
                                    {
                                        UserID = studentlist[i].UserID,
                                        BatchID = studentlist[i].BatchID,
                                        StateID = studentlist[i].StateID,
                                        StudentID = studentlist[i].StudentID,
                                        SubjectID = studentlist[i].SubjectID,
                                        FatherName = studentlist[i].FatherName,
                                        MobileNumber = studentlist[i].MobileNumber,
                                        MotherName = studentlist[i].MotherName,
                                        GenderID = studentlist[i].GenderID,
                                        Name = studentlist[i].Name,
                                        Village = studentlist[i].Village,
                                        Image = studentlist[i].Image
                                    };
                                    stdlist.Add(studentRegistration);
                                }
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        //lblmsg.Text = ex.Message.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }
            return Json(stdlist, JsonRequestBehavior.AllowGet);
        }

        #endregion


        #region Login With Google, Facebook, Linkdin, Twitter etc
        public ActionResult ExternalLogin()
        {
            return View();
        }

       
        public JsonResult testLiginData(string id,string name)
        {
            return null;
        }
        #endregion


        public ActionResult TestLogin()
        {
            return View();
        }
    }
}
﻿using Rest_Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;

namespace Rest_Api.Controllers
{
  
    public class RestAPISController : ApiController
    {
        TestingAPIEntities _TestingAPIDatabase = new TestingAPIEntities();
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new  string[]{"Value 1","Value 2" };
        }
              
       
        [HttpPost]
        public IEnumerable<string> CheckCredantial(LoginModel model)
        {
            string Tokens = "";
            var adminList = _TestingAPIDatabase.AdminLogins.Where(x => x.AdminUserName == model.UserName && x.AdminPassword == model.Password).ToList();
            if(adminList.Count!=0)
            {
                Tokens = adminList[0].AdminToken;
            }
            else
            {
                Tokens = "Invalid user name or password.";
            }
            return new string[] {Tokens};
        }
       
        private const string _alg = "HmacSHA256";
        private const string _salt = "rz8LuOtFBXphj9WQfvFh"; // Generated at https://www.random.org/strings

        public static string GenerateToken(string username, string password)
        {
            string hash = string.Join(":", new string[] { username });
            string hashLeft = "";
            string hashRight = "";

            using (HMAC hmac = HMACSHA256.Create(_alg))
            {
                hmac.Key = Encoding.UTF8.GetBytes(GetHashedPassword(password));
                hmac.ComputeHash(Encoding.UTF8.GetBytes(hash));

                hashLeft = Convert.ToBase64String(hmac.Hash);
                hashRight = string.Join(":", new string[] { username });
            }
            var tokens = Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Join(":", hashLeft, hashRight)));
            return tokens;
        }

        public static string GetHashedPassword(string password)
        {
            string key = string.Join(":", new string[] { password, _salt });

            using (HMAC hmac = HMACSHA256.Create(_alg))
            {
                // Hash the key.
                hmac.Key = Encoding.UTF8.GetBytes(_salt);
                hmac.ComputeHash(Encoding.UTF8.GetBytes(key));

                return Convert.ToBase64String(hmac.Hash);
            }
        }

        #region New User Registeration
          
        [Route("api/RestAPIS/UserRegistration")]
        [HttpPost]
        public IEnumerable<string> UserRegistration(UserLogin usermodel)
        {
          var token=  GenerateToken(usermodel.UserName, usermodel.UserPassword);
            usermodel.UserToken = token;
            usermodel.UserRegDate = DateTime.Now.ToShortDateString();
            _TestingAPIDatabase.UserLogins.Add(usermodel);
            _TestingAPIDatabase.SaveChanges();
            return new string[] {token };
        }
        #endregion
    }
}

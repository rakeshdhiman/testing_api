﻿using Rest_Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;


namespace Rest_Api.Controllers
{
    [RoutePrefix("api/UserRegistration")]
    public class UserRegistrationController : ApiController
    {
        LANiPadEntities _TestingAPIDatabase = new LANiPadEntities();

        #region New User Registeration
        [HttpPost]
        [ActionName("RegisterNewUser")]
        public IEnumerable<string> UserRegistration(UserLogin usermodel)
        {
            string _token = "";
            try
            {
                var chkData = _TestingAPIDatabase.UserLogins.Where(x => x.UserName == usermodel.UserName && x.UserMobileNumber == usermodel.UserMobileNumber).ToList();
                if(chkData.Count==0)
                {
                var token = GenerateToken(usermodel.UserName, usermodel.UserPassword);
                usermodel.UserToken = token;
                usermodel.UserRegDate = DateTime.Now.ToShortDateString();
                _token = token;
                _TestingAPIDatabase.UserLogins.Add(usermodel);
                _TestingAPIDatabase.SaveChanges();
                }
                else
                {
                    _token = "You are already Registered.Please Login";
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
           
            return new string[] { _token };
        }
        private const string _alg = "HmacSHA256";
        private const string _salt = "rz8LuOtFBXphj9WQfvFh"; // Generated at https://www.random.org/strings

        public static string GenerateToken(string username, string password)
        {
            string hash = string.Join(":", new string[] { username });
            string hashLeft = "";
            string hashRight = "";

            using (HMAC hmac = HMACSHA256.Create(_alg))
            {
                hmac.Key = Encoding.UTF8.GetBytes(GetHashedPassword(password));
                hmac.ComputeHash(Encoding.UTF8.GetBytes(hash));

                hashLeft = Convert.ToBase64String(hmac.Hash);
                hashRight = string.Join(":", new string[] { username });
            }
            var tokens = Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Join(":", hashLeft, hashRight)));
            return tokens;
        }

        public static string GetHashedPassword(string password)
        {
            string key = string.Join(":", new string[] { password, _salt });

            using (HMAC hmac = HMACSHA256.Create(_alg))
            {
                // Hash the key.
                hmac.Key = Encoding.UTF8.GetBytes(_salt);
                hmac.ComputeHash(Encoding.UTF8.GetBytes(key));

                return Convert.ToBase64String(hmac.Hash);
            }
        }
        #endregion

        #region Get User Token For Login
        // [Route("api/UserRegistration/GetSavedToken")]
        [HttpPost]
        [ActionName("GetSavedtokens")]
        public UserLogin GetSavedToken(LoginModel model)
        {
            UserLogin returnData = new UserLogin();
            string token = "";
            try
            {
                var userList = _TestingAPIDatabase.UserLogins.Where(x => x.UserName == model.UserName && x.UserPassword == model.Password).ToList();
                if(userList.Count==0)
                {
                    token = "You are not registered User, Please SignUp.";
                    
                }
                else
                {
                    returnData.UserID = userList[0].UserID;
                    returnData.UserMobileNumber = userList[0].UserMobileNumber;
                    returnData.UserName = userList[0].UserName;
                    returnData.UserToken = userList[0].UserToken;
                    returnData.UserEmailID = userList[0].UserEmailID;
                    returnData.UserPassword = userList[0].UserPassword;
                    returnData.UserRegDate = userList[0].UserRegDate;
                   
                }
            }
            catch(Exception ex)
            {
                string st = ex.Message.ToString();
            }
            return   returnData ;
        }
        #endregion

        #region New Student Registration
        [HttpPost]
        [ActionName("AddNewStudent")]
        public int AddNewStudents(StudentRegistration model)
        {
            int status = 0;
            try
            {
                _TestingAPIDatabase.StudentRegistrations.Add(model);
                _TestingAPIDatabase.SaveChanges();
                status = 1;
            }
            catch(Exception ex)
            {
                string st = ex.Message.ToString();
                status = 0;
            }
            return status;
        }
        #endregion

        #region Get Student List By User ID
        [HttpPost]
        [ActionName("GetStudentListByUserID")]
        public List<StudentRegistration> GetStudentList(StudentRegistration model)
        {
            List<StudentRegistration> stdList = new List<StudentRegistration>();
            int status = 0;
            try
            {
                var studentlist = _TestingAPIDatabase.StudentRegistrations.Where(x => x.UserID == model.UserID).ToList();
                if(studentlist.Count==0)
                {

                }
                else
                {
                    for(int i=0;i<studentlist.Count;i++)
                    {
                        StudentRegistration studentRegistration = new StudentRegistration()
                        {
                            UserID=studentlist[i].UserID,
                            BatchID=studentlist[i].BatchID,
                            StateID=studentlist[i].StateID,
                            StudentID=studentlist[i].StudentID,
                            SubjectID=studentlist[i].SubjectID,
                            FatherName=studentlist[i].FatherName,
                            MobileNumber=studentlist[i].MobileNumber,
                            MotherName=studentlist[i].MotherName,
                            GenderID=studentlist[i].GenderID,
                            Name=studentlist[i].Name,
                            Village=studentlist[i].Village,
                            Image=studentlist[i].Image
                        };
                        stdList.Add(studentRegistration);
                    }
                }
             
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
                status = 0;
            }
            return stdList;
        }
        #endregion

        #region Get Student Info By Student ID
        [HttpPost]
        [ActionName("GetStudentInfoByStudentID")]
        public List<StudentRegistration> GetStudentInfo(StudentRegistration model)
        {
            List<StudentRegistration> stdList = new List<StudentRegistration>();
            int status = 0;
            try
            {
                var studentlist = _TestingAPIDatabase.StudentRegistrations.Where(x => x.StudentID == model.StudentID).ToList();
                if (studentlist.Count == 0)
                {

                }
                else
                {
                    for (int i = 0; i < studentlist.Count; i++)
                    {
                        StudentRegistration studentRegistration = new StudentRegistration()
                        {
                            UserID = studentlist[i].UserID,
                            BatchID = studentlist[i].BatchID,
                            StateID = studentlist[i].StateID,
                            StudentID = studentlist[i].StudentID,
                            SubjectID = studentlist[i].SubjectID,
                            FatherName = studentlist[i].FatherName,
                            MobileNumber = studentlist[i].MobileNumber,
                            MotherName = studentlist[i].MotherName,
                            GenderID = studentlist[i].GenderID,
                            Name = studentlist[i].Name,
                            Village = studentlist[i].Village,
                            Image = studentlist[i].Image
                        };
                        stdList.Add(studentRegistration);
                    }
                }

            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
                status = 0;
            }
            return stdList;
        }
        #endregion

        #region Get Student List By Student Name
        [HttpPost]
        [ActionName("GetStudentListByName")]
        public List<StudentRegistration> GetStudentInfoByName(StudentRegistration model)
        {
            List<StudentRegistration> stdList = new List<StudentRegistration>();
            stdList.Clear();
            int status = 0;
            try
            {
                var studentlist = _TestingAPIDatabase.StudentRegistrations.Where(x => x.UserID==model.UserID && x.Name.Contains(model.Name)).ToList();
                if (studentlist.Count == 0)
                {

                }
                else
                {
                    for (int i = 0; i < studentlist.Count; i++)
                    {
                        StudentRegistration studentRegistration = new StudentRegistration()
                        {
                            UserID = studentlist[i].UserID,
                            BatchID = studentlist[i].BatchID,
                            StateID = studentlist[i].StateID,
                            StudentID = studentlist[i].StudentID,
                            SubjectID = studentlist[i].SubjectID,
                            FatherName = studentlist[i].FatherName,
                            MobileNumber = studentlist[i].MobileNumber,
                            MotherName = studentlist[i].MotherName,
                            GenderID = studentlist[i].GenderID,
                            Name = studentlist[i].Name,
                            Village = studentlist[i].Village,
                            Image = studentlist[i].Image
                        };
                        stdList.Add(studentRegistration);
                    }
                }

            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
                status = 0;
            }
            return stdList;
        }
        #endregion

        #region Get Student List By Mobile Number 
        [HttpPost]
        [ActionName("GetStudentListByMobileNumber")]
        public List<StudentRegistration> GetStudentInfoByMobile(StudentRegistration model)
        {
            List<StudentRegistration> stdList = new List<StudentRegistration>();
            stdList.Clear();
            int status = 0;
            try
            {
                var studentlist = _TestingAPIDatabase.StudentRegistrations.Where(x => x.UserID == model.UserID && x.MobileNumber.Contains(model.MobileNumber)).ToList();
                if (studentlist.Count == 0)
                {

                }
                else
                {
                    for (int i = 0; i < studentlist.Count; i++)
                    {
                        StudentRegistration studentRegistration = new StudentRegistration()
                        {
                            UserID = studentlist[i].UserID,
                            BatchID = studentlist[i].BatchID,
                            StateID = studentlist[i].StateID,
                            StudentID = studentlist[i].StudentID,
                            SubjectID = studentlist[i].SubjectID,
                            FatherName = studentlist[i].FatherName,
                            MobileNumber = studentlist[i].MobileNumber,
                            MotherName = studentlist[i].MotherName,
                            GenderID = studentlist[i].GenderID,
                            Name = studentlist[i].Name,
                            Village = studentlist[i].Village,
                            Image = studentlist[i].Image
                        };
                        stdList.Add(studentRegistration);
                    }
                }

            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
                status = 0;
            }
            return stdList;
        }
        #endregion
    }
}

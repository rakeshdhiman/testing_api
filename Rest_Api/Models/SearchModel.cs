﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rest_Api.Models
{
    public class SearchModel
    {
        public string UserID { get; set; }
        public string Name { get; set; }
        public string MobileNumber { get; set; }
    }
}
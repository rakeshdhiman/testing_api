﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rest_Api.Models
{
    public class LoginModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}